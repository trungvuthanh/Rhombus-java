import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HinhThoiTestBound {

    @Test
    void test1() {
        assertEquals("null", new Rhombus().getRhombus(1, 1, 2, 2, 3, 3));
    }

    @Test
    void test2() {
        assertEquals("(2, 5)", new Rhombus().getRhombus(2, 1, 1, 3, 3, 3));
    }

    @Test
    void test3() {
        assertEquals("(3, 3)", new Rhombus().getRhombus(2, 1, 1, 3, 2, 5));
    }

//    @Test
//    void test4() {
//        assertEquals("(3, 3)", new Rhombus().getRhombus(2, 1, 1, 3, 2, 5));
//    }
//
//    @Test
//    void test5() {
//        assertEquals("(2, 5)", new Rhombus().getRhombus(1, 3, 3, 3, 2, 1));
//    }
//
//    @Test
//    void test6() {
//        assertEquals("null", new Rhombus().getRhombus(1, 1, 1, 2, 4, 1));
//    }

//    @Test
//    void test7() {
//        assertEquals("null", new Rhombus().getRhombus(56, 320, 225, 790, 500, 1000));
//    }

}