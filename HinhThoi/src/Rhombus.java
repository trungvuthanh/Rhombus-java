class Point {
    int x;
    int y;
    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
}

public class Rhombus {

    String getRhombus (int aX, int aY, int bX, int bY, int cX, int cY) {
        if ((aX >= 0 && aX <= 1000) && (bX >= 0 && bX <= 1000) && (cX >= 0 && cX <= 1000) && (aY >= 0 && aY <= 1000) && (bY >= 0 && bY <= 1000) && (cY >= 0 && cY <= 1000)) {
            Point a = new Point(aX, aY);
            Point b = new Point(bX, bY);
            Point c = new Point(cX, cY);

            // Kiểm tra 3 điểm thẳng hàng
            int temp1 = b.y - a.y;
            int temp2 = a.x - b.x;
            int temp3 = temp1 * a.x + temp2 * a.y;
            if (temp1 * c.x + temp2 * c.y == temp3) {
                return "null";
            } else {
                // Kiểm tra tam giác cân
                double firstEdge = Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)); // Cạnh AB
                double secondEdge = Math.sqrt((a.x - c.x) * (a.x - c.x) + (a.y - c.y) * (a.y - c.y)); // Cạnh AC
                double thirdEdge = Math.sqrt((c.x - b.x) * (c.x - b.x) + (c.y - b.y) * (c.y - b.y)); // Cạnh BC
                if (firstEdge == secondEdge) { // AB = AC
                    // Tọa độ đỉnh thứ 4 tạo nên hình thoi
                    int dX = b.x + c.x - a.x;
                    int dY = b.y + c.y - a.y;
                    return ((dX >= 0 && dX <= 1000) && (dY >= 0 && dY <= 1000)) ? "(" + dX + ", " + dY + ")" : "null";
                }
                else if (firstEdge == thirdEdge) { // AB = BC
                    // Tọa độ đỉnh thứ 4 tạo nên hình thoi
                    int dX = a.x + c.x - b.x;
                    int dY = a.y + c.y - b.y;
                    return ((dX >= 0 && dX <= 1000) && (dY >= 0 && dY <= 1000)) ? "(" + dX + ", " + dY + ")" : "null";
                }
                else if (secondEdge == thirdEdge) { // AC = BC
                    // Tọa độ đỉnh thứ 4 tạo nên hình thoi
                    int dX = a.x + b.x - c.x;
                    int dY = a.y + b.y - c.y;
                    return ((dX >= 0 && dX <= 1000) && (dY >= 0 && dY <= 1000)) ? "(" + dX + ", " + dY + ")" : "null";
                }
            }
        }
        return "null";
    }

}
